package br.edu.unisep.medicines.utils

const val UNIT_PILLS = 0
const val UNIT_DROPS = 1

const val DOSAGE_ON_TIME = 0
const val DOSAGE_LATE = 1
const val DOSAGE_EARLY = 2
