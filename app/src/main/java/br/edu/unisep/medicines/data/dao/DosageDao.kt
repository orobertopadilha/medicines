package br.edu.unisep.medicines.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.edu.unisep.medicines.data.entity.Dosage

@Dao
interface DosageDao {

    @Insert
    suspend fun save(dosage: Dosage)

    @Query("select * from dosages where medicineId = :medicineId order by id desc limit 20")
    suspend fun findByMedicine(medicineId: Int): List<Dosage>

}