package br.edu.unisep.medicines.ui.dosage.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.medicines.dto.dosage.DosageDto
import br.edu.unisep.medicines.dto.dosage.RegisterDosageDto
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.repository.DosageRepository
import br.edu.unisep.medicines.utils.DOSAGE_EARLY
import br.edu.unisep.medicines.utils.DOSAGE_LATE
import br.edu.unisep.medicines.utils.DOSAGE_ON_TIME
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class MedicineDosageViewModel : ViewModel() {

    lateinit var medicine: MedicineDto

    var dosageList = MutableLiveData<List<DosageDto>>()
    var registerResult = MutableLiveData<Unit>()

    private val repository = DosageRepository()

    fun findDosages() {
        viewModelScope.launch {
            val dosages = repository.findByMedicine(medicine.id)
            dosageList.postValue(dosages)
        }
    }

    fun save() {
        val now = LocalDateTime.now()
        val lastDosage = dosageList.value?.firstOrNull()?.date

        val diff = lastDosage?.until(now, ChronoUnit.HOURS) ?: medicine.frequency

        val status = when {
            diff.toInt() == medicine.frequency -> DOSAGE_ON_TIME
            diff.toInt() > medicine.frequency -> DOSAGE_LATE
            else -> DOSAGE_EARLY
        }

        val dosage = RegisterDosageDto(medicine.id, now, status)
        viewModelScope.launch {
            repository.save(dosage)
            registerResult.postValue(Unit)
        }
    }

}