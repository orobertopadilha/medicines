package br.edu.unisep.medicines.dto.dosage

import java.time.LocalDateTime

data class RegisterDosageDto(
    val medicineId: Int,
    val date: LocalDateTime,
    val status: Int
)
