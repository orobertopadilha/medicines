package br.edu.unisep.medicines.utils

import android.content.Context
import br.edu.unisep.medicines.R
import br.edu.unisep.medicines.dto.medicine.MedicineDto

fun getInstructions(context: Context, medicine: MedicineDto): String {
    val unit = context.getString(getUnit(medicine.unity))
    return context.getString(
        R.string.itemInstructions,
        medicine.dosage, unit, medicine.frequency
    )
}

private fun getUnit(unit: Int) =
    if (unit == UNIT_PILLS) {
        R.string.unitPills
    } else {
        R.string.unitDrops
    }