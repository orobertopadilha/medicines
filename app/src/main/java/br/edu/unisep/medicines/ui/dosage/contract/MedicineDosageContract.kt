package br.edu.unisep.medicines.ui.dosage.contract

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.ui.dosage.MedicineDosageActivity

class MedicineDosageContract : ActivityResultContract<MedicineDto, Unit>() {

    override fun createIntent(context: Context, input: MedicineDto?) =
        Intent(context, MedicineDosageActivity::class.java)
            .putExtra(EXTRA_MEDICINE, input)

    override fun parseResult(resultCode: Int, intent: Intent?) = Unit

    companion object {
        const val EXTRA_MEDICINE = "medicine"
    }
}