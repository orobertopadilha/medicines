package br.edu.unisep.medicines.ui.dosage

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.unisep.medicines.databinding.ActivityMedicineDosageBinding
import br.edu.unisep.medicines.dto.dosage.DosageDto
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.ui.dosage.adapter.MedicineDosageAdapter
import br.edu.unisep.medicines.ui.dosage.contract.MedicineDosageContract.Companion.EXTRA_MEDICINE
import br.edu.unisep.medicines.ui.dosage.viewmodel.MedicineDosageViewModel
import br.edu.unisep.medicines.utils.getInstructions

class MedicineDosageActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMedicineDosageBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<MedicineDosageViewModel>()
    private lateinit var adapter: MedicineDosageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        initialize()
    }

    private fun initialize() {
        val medicine = intent.getSerializableExtra(EXTRA_MEDICINE) as MedicineDto
        viewModel.medicine = medicine

        binding.tvMedicineName.text = viewModel.medicine.name
        binding.tvInstructions.text = getInstructions(this, viewModel.medicine)

        setupList()
        setupListeners()

        viewModel.findDosages()
    }

    private fun setupListeners() {
        viewModel.dosageList.observe(this, ::onDosagesLoaded)
        viewModel.registerResult.observe(this, ::onRegisterResult)

        binding.btnRegisterDosage.setOnClickListener { viewModel.save() }
    }

    private fun onDosagesLoaded(dosages: List<DosageDto>) {
        adapter.dosages = dosages

        binding.pbDosages.visibility = View.GONE
        binding.rvDosages.visibility = View.VISIBLE
    }

    private fun onRegisterResult(result: Unit) {
        binding.rvDosages.visibility = View.GONE
        binding.pbDosages.visibility = View.VISIBLE

        viewModel.findDosages()
    }

    private fun setupList() {
        adapter = MedicineDosageAdapter()

        binding.rvDosages.adapter = adapter
        binding.rvDosages.layoutManager = LinearLayoutManager(this)
        binding.rvDosages.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }
}