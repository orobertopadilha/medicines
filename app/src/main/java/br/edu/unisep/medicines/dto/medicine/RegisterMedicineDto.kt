package br.edu.unisep.medicines.dto.medicine

data class RegisterMedicineDto(
    val name: String,
    val dosage: Int,
    val unity: Int,
    val frequency: Int
)
