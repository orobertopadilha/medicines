package br.edu.unisep.medicines.ui.register.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.medicines.dto.medicine.RegisterMedicineDto
import br.edu.unisep.medicines.repository.MedicineRepository
import kotlinx.coroutines.launch

class RegisterMedicineViewModel : ViewModel() {

    private val repository = MedicineRepository()

    val onSaveComplete = MutableLiveData<Unit>()
    var unity: Int = 0

    fun save(medicine: RegisterMedicineDto) {
        viewModelScope.launch {
            repository.save(medicine)
            onSaveComplete.postValue(Unit)
        }
    }

}