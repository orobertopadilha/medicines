package br.edu.unisep.medicines.ui.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import br.edu.unisep.medicines.R
import br.edu.unisep.medicines.databinding.ActivityRegisterMedicineBinding
import br.edu.unisep.medicines.dto.medicine.RegisterMedicineDto
import br.edu.unisep.medicines.ui.register.viewmodel.RegisterMedicineViewModel

class RegisterMedicineActivity : AppCompatActivity() {

    private val viewModel by viewModels<RegisterMedicineViewModel>()

    private val binding by lazy {
        ActivityRegisterMedicineBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupUnitSpinner()
        viewModel.onSaveComplete.observe(this, { onSaveComplete() })
    }

    private fun setupUnitSpinner() {
        val adapter = ArrayAdapter.createFromResource(
            this, R.array.unitOptions,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spUnit.onItemSelectedListener = unitSelectionListener
        binding.spUnit.adapter = adapter
    }

    private fun save() {
        if (validateInput()) {
            viewModel.save(
                RegisterMedicineDto(
                    binding.etName.text.toString(),
                    binding.etDosage.text.toString().toInt(),
                    viewModel.unity,
                    binding.etFrequency.text.toString().toInt()
                )
            )
        }
    }

    private fun validateInput(): Boolean {
        var isValid = true

        binding.tilName.error = if (binding.etName.text.toString().trim().isEmpty()) {
            isValid = false
            getString(R.string.messageNameRequired)
        } else {
            null
        }

        binding.tilDosage.error = if (binding.etDosage.text.toString().trim().isEmpty()) {
            isValid = false
            getString(R.string.messageRequired)
        } else {
            null
        }

        binding.tilFrequency.error = if (binding.etFrequency.text.toString().trim().isEmpty()) {
            isValid = false
            getString(R.string.messageRequired)
        } else {
            null
        }

        return isValid
    }

    private fun onSaveComplete() {
        setResult(RESULT_OK)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_register_medicine, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnSave) {
            save()
            return true
        }

        return false
    }

    private val unitSelectionListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(
            spinner: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ) {
            viewModel.unity = position
        }

        override fun onNothingSelected(spinner: AdapterView<*>?) {
            // Nao implementado
        }
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, RegisterMedicineActivity::class.java)
    }
}