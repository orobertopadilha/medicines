package br.edu.unisep.medicines.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.medicines.R
import br.edu.unisep.medicines.databinding.ItemMedicineBinding
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.utils.getInstructions

class ListMedicineAdapter : RecyclerView.Adapter<ListMedicineAdapter.MedicineViewHolder>() {

    lateinit var onItemSelected: (MedicineDto) -> Unit

    var items = listOf<MedicineDto>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_medicine, parent, false)
        return MedicineViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MedicineViewHolder, position: Int) {
        holder.bindItem(items[position], onItemSelected)
    }

    override fun getItemCount() = items.size

    inner class MedicineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemMedicineBinding.bind(itemView)

        fun bindItem(medicine: MedicineDto, onItemSelected: (MedicineDto) -> Unit) {
            binding.tvMedicine.text = medicine.name
            binding.tvInstructions.text = getInstructions(itemView.context, medicine)
            binding.root.setOnClickListener { onItemSelected(medicine) }
        }

    }
}