package br.edu.unisep.medicines.ui.list.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.repository.MedicineRepository
import kotlinx.coroutines.launch

class ListMedicineViewModel : ViewModel() {

    private val repository = MedicineRepository()

    val listMedicine = MutableLiveData<List<MedicineDto>>()

    fun list() {
        viewModelScope.launch {
            val result = repository.findAll()
            listMedicine.postValue(result)
        }
    }

}