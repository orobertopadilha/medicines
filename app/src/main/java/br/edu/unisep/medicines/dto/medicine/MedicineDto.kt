package br.edu.unisep.medicines.dto.medicine

import java.io.Serializable

data class MedicineDto(
    val id: Int,
    val name: String,
    val dosage: Int,
    val unity: Int,
    val frequency: Int
) : Serializable
