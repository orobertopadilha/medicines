package br.edu.unisep.medicines.dto.dosage

import java.time.LocalDateTime

data class DosageDto(
    val date: LocalDateTime,
    val status: Int
)
