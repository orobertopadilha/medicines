package br.edu.unisep.medicines.ui.dosage.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.medicines.R
import br.edu.unisep.medicines.databinding.ItemDosageBinding
import br.edu.unisep.medicines.dto.dosage.DosageDto
import br.edu.unisep.medicines.utils.DOSAGE_EARLY
import br.edu.unisep.medicines.utils.DOSAGE_ON_TIME
import java.time.format.DateTimeFormatter

class MedicineDosageAdapter : RecyclerView.Adapter<MedicineDosageAdapter.MedicineDosageViewHolder>(){

    var dosages = listOf<DosageDto>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineDosageViewHolder {
        val item = LayoutInflater.from(parent.context).inflate(R.layout.item_dosage, parent, false)
        return MedicineDosageViewHolder(item)
    }

    override fun onBindViewHolder(holder: MedicineDosageViewHolder, position: Int) {
        holder.bind(dosages[position])
    }

    override fun getItemCount() = dosages.size

    inner class MedicineDosageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemDosageBinding.bind(itemView)

        fun bind(dosage: DosageDto) {
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            binding.tvDosageDate.text = dosage.date.format(formatter)

            when (dosage.status) {
                DOSAGE_ON_TIME -> {
                    binding.tvDosageStatus.setTextColor(itemView.context.getColor(R.color.greenText))
                    binding.tvDosageStatus.setText(R.string.dosageOnTime)
                }
                DOSAGE_EARLY -> {
                    binding.tvDosageStatus.setTextColor(itemView.context.getColor(R.color.orangeText))
                    binding.tvDosageStatus.setText(R.string.dosageEarly)
                }
                else -> {
                    binding.tvDosageStatus.setTextColor(itemView.context.getColor(R.color.redText))
                    binding.tvDosageStatus.setText(R.string.dosageLate)
                }
            }
        }
    }

}