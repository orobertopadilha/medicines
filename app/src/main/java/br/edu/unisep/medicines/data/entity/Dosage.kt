package br.edu.unisep.medicines.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dosages")
data class Dosage(
    val medicineId: Int,
    val date: String,
    val status: Int,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}
