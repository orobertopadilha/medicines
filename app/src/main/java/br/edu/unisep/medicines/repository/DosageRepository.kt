package br.edu.unisep.medicines.repository

import br.edu.unisep.medicines.data.db.MedicineDb
import br.edu.unisep.medicines.data.entity.Dosage
import br.edu.unisep.medicines.dto.dosage.DosageDto
import br.edu.unisep.medicines.dto.dosage.RegisterDosageDto
import java.time.LocalDateTime

class DosageRepository {

    private val dao = MedicineDb.instance.dosageDao()

    suspend fun findByMedicine(medicineId: Int): List<DosageDto> {
        val dosages = dao.findByMedicine(medicineId)

        return dosages.map { dosage ->
            DosageDto(LocalDateTime.parse(dosage.date), dosage.status)
        }
    }

    suspend fun save(registerDosage: RegisterDosageDto) {
        val dosage = Dosage(registerDosage.medicineId, registerDosage.date.toString(), registerDosage.status)
        dao.save(dosage)
    }

}