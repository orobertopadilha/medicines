package br.edu.unisep.medicines.repository

import br.edu.unisep.medicines.data.db.MedicineDb
import br.edu.unisep.medicines.data.entity.Medicine
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.dto.medicine.RegisterMedicineDto

class MedicineRepository {

    private val dao = MedicineDb.instance.medicineDao()

    suspend fun save(medicine: RegisterMedicineDto) {
        val entity = Medicine(medicine.name, medicine.dosage, medicine.unity, medicine.frequency)
        dao.save(entity)
    }

    suspend fun findAll(): List<MedicineDto> {
        val medicines = dao.findAll()

        return medicines.map { medicine ->
            MedicineDto(
                medicine.id ?: 0,
                medicine.name,
                medicine.dosage,
                medicine.unity,
                medicine.frequency
            )
        }
    }
}