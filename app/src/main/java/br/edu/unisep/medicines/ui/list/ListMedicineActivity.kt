package br.edu.unisep.medicines.ui.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.unisep.medicines.R
import br.edu.unisep.medicines.databinding.ActivityListMedicineBinding
import br.edu.unisep.medicines.dto.medicine.MedicineDto
import br.edu.unisep.medicines.ui.dosage.contract.MedicineDosageContract
import br.edu.unisep.medicines.ui.list.adapter.ListMedicineAdapter
import br.edu.unisep.medicines.ui.list.viewmodel.ListMedicineViewModel
import br.edu.unisep.medicines.ui.register.RegisterMedicineActivity

class ListMedicineActivity : AppCompatActivity() {

    private val viewModel: ListMedicineViewModel by viewModels()

    private lateinit var adapter: ListMedicineAdapter

    private val binding: ActivityListMedicineBinding by lazy {
        ActivityListMedicineBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupList()
        setupEvents()
    }

    private fun setupList() {
        adapter = ListMedicineAdapter()
        adapter.onItemSelected = ::onMedicineSelected

        binding.rvMedicines.adapter = adapter
        binding.rvMedicines.layoutManager = LinearLayoutManager(this)
        binding.rvMedicines.addItemDecoration(
            DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun setupEvents() {
        viewModel.listMedicine.observe(this) { medicines ->
            this.adapter.items = medicines
        }

        viewModel.list()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_list_medicine, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnAdd) {
            registerResult.launch(RegisterMedicineActivity.newIntent(this))
            return true
        }

        return false
    }

    private fun onMedicineSelected(medicine: MedicineDto) {
        dosageResult.launch(medicine)
    }

    private val registerResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            viewModel.list()
        }
    }

    private val dosageResult = registerForActivityResult(MedicineDosageContract()) {
        viewModel.list()
    }

}